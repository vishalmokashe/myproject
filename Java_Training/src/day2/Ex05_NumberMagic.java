package day2;

import java.util.Scanner;

public class Ex05_NumberMagic {
    public static void main(String[] args) {

        int num , reversedNum = 0, diff1=0, RevDiff1=0, sum=0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a number : ");
        num=sc.nextInt();
        int orig_num = num;
        for(int i=0; num>0;i++) {
            int digit = num % 10;
            reversedNum = reversedNum * 10 + digit;
            num /= 10;
        }
        System.out.println("Reversed Number: " + reversedNum);
        diff1 = reversedNum - orig_num;
        System.out.println("Diff1 value is : "+diff1);
        int orig_diff1 = diff1;

        for(int i=0; diff1>0;i++) {
            int digit = diff1 % 10;
            RevDiff1 = RevDiff1 * 10 + digit;
            diff1 /= 10;
        }
        System.out.println("Reversed Number: " + RevDiff1);

        sum =  orig_diff1 + RevDiff1;
        System.out.println("Sum = " + sum);
    }
}
