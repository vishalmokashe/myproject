package day2;

import java.util.Scanner;

public class TableOfNumber {
    public static void main(String[] args) {
        int n, c, i;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a number for table : ");
        n=sc.nextInt();
        for (c = 1; c <= 10; c++) {
            i=n*c;
            System.out.println(n + " * " + c + " = " +i);
        }
    }
}
