package util;

public class UtilityClass {

    public static boolean isPrime(int num) {
        boolean retcode=false;
        boolean flag=false;
        int i;
        if (num % 2 != 0) {
            for (i = 2; i <= num / 2; ++i) {
                // condition for nonprime number
                if (num % i == 0) {
                    flag = true;
                    break;
                }
            }
        }
            if (!flag) {
                retcode = true;
            }
            return retcode;
        }
    }

