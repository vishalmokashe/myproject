package day5;

import util.UtilityClass;

public class Ex06_EvenOddPrime {

    public static void main (String args[]) { //Temporary comment
        int num = 15;
        boolean flag = false;
        int i=0;
        if (num % 2 == 0) {
            System.out.println("Number is even : " +num );
            System.out.println("Square of even no is : " +num*num );
        }

        else if(UtilityClass.isPrime(num)){
            System.out.println("Number "+num+ " is a prime number.");
            System.out.println("Table for number "+num +" is :");
            for (int c = 1; c <= 10; c++) {
                i = num * c;
                System.out.println(num + " * " + c + " = " + i);
            }
        }
        else{
            System.out.println("Number "+num+" is odd.");
            System.out.println("Cube of "+num+" is : " + num * num * num);

        }
     /*   else if (num % 2 != 0) {
            for (i = 2; i <= num / 2; ++i) {
                // condition for nonprime number
                if (num % i == 0) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                System.out.println("Number is a prime number :"+num);
                System.out.println("Table of a prime number :");
                for (int c = 1; c <= 10; c++) {
                    i = num * c;
                    System.out.println(num + " * " + c + " = " + i);
                }
            }
            else{
                System.out.println("Number is odd : " + num);
                System.out.println("Cube of odd no is : " + num * num * num);
            }
        }*/
    }


}
